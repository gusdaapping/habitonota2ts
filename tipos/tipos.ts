
//string
let nome = "jonas"
console.log(nome)

nome = 28;
console.log(nome)


//numbers
let idade = 27
console.log(idade)
idade = "ana";
console.log(idade)

//boolean
let possuiHobbies = false
console.log(possuiHobbies)
possuiHobbies = 1;
console.log(possuiHobbies)

//tipos explicitos
let minhaIdade:any
console.log(typeof minhaIdade)
console.log(minhaIdade)
minhaIdade = 27
console.log(typeof minhaIdade)
console.log(minhaIdade)
minhaIdade = "27"
console.log(typeof minhaIdade)
console.log(minhaIdade)

let hobbies: (string|number)[] = [2,1,"coisa 1","coisa 2",5];
console.log(typeof hobbies)

let hobbies2: (string|number)[] = ["coisa 1","coisa 2"];

let hobbies3:[string, number, string, number, string] = [2,1,"coisa 1","coisa 2",5];


//enums -> tipo
enum Semana{
    segunda,
    terca,
    quarta,
    quinta,
    sexta,
    sabado,
    domingo
}

let segunda:Semana = Semana.terca
console.log(segunda)

//any
let carro: any = "mnw"
console.log(carro)
carro = {marca:"mbw" , ano: "2019"}
console.log(carro)

//funcoes
function retornaMeuNome():object{
    let meuObjeto = {nome:"nome" , minhaIdade:"idade"}
    return meuObjeto
}

console.log(retornaMeuNome());

function x(): void {
        console.log("oi")
        
        }
x();

let multiplicar = function (numA:number,numB:number):number{
    return numA * numB
}
console.log(multiplicar(2,4))


function teste (){

}
//variave em funcao
const variavel = function (a: number,b:number):boolean{
    return false
}

let y;
y = x();


console.log(multiplicar)
console.log(multiplicar(9,4))

let oqAparece = document.createElement("h1");
document.body.appendChild(oqAparece)
oqAparece.innerHTML = multiplicar(9,4);


// objeto
let objeto:{teste:string, teste2:number} = {
    teste: 'joao',
    teste2: 10
}
console.log(objeto)
