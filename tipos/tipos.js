"use strict";
//string
var nome = "jonas";
console.log(nome);
nome = 28;
console.log(nome);
//numbers
var idade = 27;
console.log(idade);
idade = "ana";
console.log(idade);
//boolean
var possuiHobbies = false;
console.log(possuiHobbies);
possuiHobbies = 1;
console.log(possuiHobbies);
//tipos explicitos
var minhaIdade;
console.log(typeof minhaIdade);
console.log(minhaIdade);
minhaIdade = 27;
console.log(typeof minhaIdade);
console.log(minhaIdade);
minhaIdade = "27";
console.log(typeof minhaIdade);
console.log(minhaIdade);
var hobbies = [2, 1, "coisa 1", "coisa 2", 5];
console.log(typeof hobbies);
var hobbies2 = ["coisa 1", "coisa 2"];
var hobbies3 = [2, 1, "coisa 1", "coisa 2", 5];
//enums -> tipo
var Semana;
(function (Semana) {
    Semana[Semana["segunda"] = 0] = "segunda";
    Semana[Semana["terca"] = 1] = "terca";
    Semana[Semana["quarta"] = 2] = "quarta";
    Semana[Semana["quinta"] = 3] = "quinta";
    Semana[Semana["sexta"] = 4] = "sexta";
    Semana[Semana["sabado"] = 5] = "sabado";
    Semana[Semana["domingo"] = 6] = "domingo";
})(Semana || (Semana = {}));
var segunda = Semana.terca;
console.log(segunda);
//any
var carro = "mnw";
console.log(carro);
carro = { marca: "mbw", ano: "2019" };
console.log(carro);
//funcoes
function retornaMeuNome() {
    var meuObjeto = { nome: "nome", minhaIdade: "idade" };
    return meuObjeto;
}
console.log(retornaMeuNome());
function x() {
    console.log("oi");
}
x();
var multiplicar = function (numA, numB) {
    return numA * numB;
};
console.log(multiplicar(2, 4));
function teste() {
}
var variavel = function (a, b) {
    return false;
};
var y;
y = x();
console.log(multiplicar);
console.log(multiplicar(9, 4));
var oqAparece = document.createElement("h1");
document.body.appendChild(oqAparece);
oqAparece.innerHTML = multiplicar(9, 4);
var objeto = {
    teste: 'joao',
    teste2: 10
};
console.log(objeto);
