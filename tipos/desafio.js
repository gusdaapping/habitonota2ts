"use strict";
var funcionario = {
    supervisores: ["sup1", "sup2"],
    batePonto: function (horaRecebida) {
        if (horaRecebida <= 8) {
            return "No Horario";
        }
        else {
            return "Fora do Horario";
        }
    }
};
console.log(funcionario.batePonto(7));
console.log(funcionario.batePonto(9));
console.log(funcionario.supervisores);
